using System;
using _Project.Scripts.Architecture.Services;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class CompleteWindow : WindowBase
    {
        [SerializeField] private Button _openOptionsButton;

        public override void Initialize()
        {
            base.Initialize();
            _openOptionsButton.onClick.AddListener(OpenOptions);
        }

        private void OnDestroy()
        {
            _openOptionsButton.onClick.RemoveListener(OpenOptions);
        }

        private void OpenOptions()
        {
            _uiService.OpenWindow(WindowType.Pause, true, true);
            CloseWindow(false);
        }

        public override WindowType GetWindowType() => WindowType.CompleteWindow;
    }
}