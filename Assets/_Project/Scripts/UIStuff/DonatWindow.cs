﻿using _Project.Scripts.Architecture.Services;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Project.Scripts.UIStuff
{
    public class DonatWindow : WindowBase
    {
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _continueButton;
        [SerializeField] private IAPButton _donat1Button;
        [SerializeField] private IAPButton _donat25Button;
        [SerializeField] private IAPButton _donat100Button;
        [SerializeField] private TextMeshProUGUI _price1;
        [SerializeField] private TextMeshProUGUI _price25;
        [SerializeField] private TextMeshProUGUI _price100;
        [SerializeField] private GameObject _purchaseCompletePanel;

        void Start()
        {
            DOVirtual.DelayedCall(0.1f, () =>
                {
                    var product = CodelessIAPStoreListener.Instance.GetProduct(_donat1Button.productId);
                    var product2 = CodelessIAPStoreListener.Instance.GetProduct(_donat25Button.productId);
                    var product3 = CodelessIAPStoreListener.Instance.GetProduct(_donat100Button.productId);

                    _donat1Button.onPurchaseComplete.AddListener(OnPurchaseComplete);
                    _donat1Button.onPurchaseFailed.AddListener(OnPurchaseFailed);
                    _donat25Button.onPurchaseComplete.AddListener(OnPurchaseComplete);
                    _donat25Button.onPurchaseFailed.AddListener(OnPurchaseFailed);
                    _donat100Button.onPurchaseComplete.AddListener(OnPurchaseComplete);
                    _donat100Button.onPurchaseFailed.AddListener(OnPurchaseFailed);

                    _price1.text = product.metadata.localizedPriceString;
                    _price25.text = product2.metadata.localizedPriceString;
                    _price100.text = product3.metadata.localizedPriceString;
                }
            );
        }

        public override void Initialize()
        {
            base.Initialize();
            _purchaseCompletePanel.SetActive(false);
            _continueButton.onClick.AddListener(Continue);
            _restartButton.onClick.AddListener(Continue);
        }

        private void OnPurchaseFailed(Product arg0, PurchaseFailureReason arg1)
        {
        }

        private void OnPurchaseComplete(Product arg0)
        {
            _purchaseCompletePanel.SetActive(true);
        }

        private void Continue()
        {
            SceneManager.LoadScene("Game");
        }

        public override WindowType GetWindowType() => WindowType.DonatWindow;
    }
}