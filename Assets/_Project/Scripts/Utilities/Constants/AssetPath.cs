using UnityEngine;

namespace Assets._Project.Scripts.Utilities.Constants
{
    public static class AssetPath
    {
        public const string GlobalSettingsPath = "Settings/GlobalSettings";
        public const string WindowsPath = "UI/Windows";
    }
}