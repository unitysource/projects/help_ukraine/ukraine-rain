﻿using _Core.Scripts.Managers.Save;
using _Project.Scripts.Architecture.Services;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Mechanics;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using _Project.Scripts.Tiles;
using Assets._Project.Scripts.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace _Project.Scripts
{
    public class Core : MonoBehaviour
    {
        [SerializeField] private Transform _uiCanvas;
        [SerializeField] private Board _board;

        private IUIService _uiService;
        private DataService _dataService;
        private DiContainer _diContainer;
        private ISaveLoadService _saveLoadService;

        public Transform UICanvas => _uiCanvas;

        private void Start()
        {
            _uiService.OpenWindow(WindowType.HUD, true, false);
            var tapWindow = _uiService.OpenWindow(WindowType.TapToPlayWindow, true, true);
            tapWindow.OpenWindow(false);

            _uiService.OpenWindow(WindowType.CompleteWindow, true, false);

            _board.Initialize(this);

// #if UNITY_EDITOR
            SceneManager.sceneUnloaded += OnSceneUnloaded;
// #endif
        }

        [Inject]
        private void Construct(AssetService assetService, DataService dataService, IUIService uiService,
            DiContainer diContainer, ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
            _diContainer = diContainer;
            _dataService = dataService;
            // _assetService = assetService;
            _uiService = uiService;
            _uiService.Initialize(UICanvas);
        }

        // #if UNITY_EDITOR
        private void OnSceneUnloaded(Scene current)
        {
            QuitedCallback();
            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }
// #endif

        private void OnApplicationQuit()
        {
            QuitedCallback();
        }

        private void QuitedCallback()
        {
            CommonTools.SaveCurrentTime();
            SaveManager.SetString(SaveKey.SearchedCard, GameResourceType.None.ToString());
            _saveLoadService.SaveAllData();
        }

        public void Complete()
        {
            _saveLoadService.PlayerData.Level++;
            SceneManager.LoadScene("Complete");
            // _uiService.OpenWindow(WindowType.CompleteWindow, false, true);
        }
    }
}