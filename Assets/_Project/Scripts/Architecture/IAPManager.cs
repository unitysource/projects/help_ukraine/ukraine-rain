using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : MonoBehaviour, IStoreListener
{
    public Action OnInitialize;
    private static IAPManager _instance;

    private IStoreController controller;
    private IExtensionProvider extensions;

    public static IAPManager Instance
    {
        get => _instance;
        set => _instance = value;
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        DontDestroyOnLoad(gameObject);

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct("1_dollar", ProductType.NonConsumable);
        builder.AddProduct("25_dollars", ProductType.NonConsumable);
        builder.AddProduct("100_dollrs", ProductType.NonConsumable);
        //IAPConfigurationHelper.PopulateConfigurationBuilder(ref builder, ProductCatalog.LoadDefaultCatalog());
        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
    {
        throw new NotImplementedException();
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        this.controller = controller;
        this.extensions = extensions;
        OnInitialize?.Invoke();
    }
}