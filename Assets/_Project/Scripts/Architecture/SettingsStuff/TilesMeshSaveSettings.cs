using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Project.Scripts
{
    [Serializable]
    public class MeshTileProperties
    {
        public int TileID;

        public Vector3[] Vertices;
        public Vector2[] UVs;
        public int[] Triangles;

        public MeshTileProperties(int id, Vector3[] sharedMeshVertices, int[] sharedMeshTriangles,
            Vector2[] sharedMeshUV)
        {
            TileID = id;
            Vertices = sharedMeshVertices;
            UVs = sharedMeshUV;
            Triangles = sharedMeshTriangles;
        }
    }

    [CreateAssetMenu(fileName = "TilesMeshSaveSettings", menuName = "Data/TilesMeshSaveSettings")]
    public class TilesMeshSaveSettings : ScriptableObject
    {
        public List<MeshTileProperties> MeshTilePropertiesList;
    }
}