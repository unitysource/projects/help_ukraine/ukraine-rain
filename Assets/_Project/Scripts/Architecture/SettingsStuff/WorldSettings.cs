using System;
using _Project.Scripts.Tiles;
using NaughtyAttributes;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "WorldSettings", menuName = "Settings/WorldSettings")]
    public class WorldSettings : ScriptableObject
    {
        [SerializeField] private Color _deactivatedTileColor;
        [SerializeField] private Sprite _blueTierSprite;
        [SerializeField] private Sprite _yellowTierSprite;
        
        [SerializeField, Range(0.001f, 5f)] private float _connectDuration;
        [SerializeField, Range(3f, 100f)] private float _maxTilesMoveSpeed;

        public float ConnectDuration => _connectDuration;
        

        public float MAXTilesMoveSpeed => _maxTilesMoveSpeed;

        public Sprite YellowTierSprite => _yellowTierSprite;

        public Sprite BlueTierSprite => _blueTierSprite;

        public Color DeactivatedTileColor => _deactivatedTileColor;
    }
}