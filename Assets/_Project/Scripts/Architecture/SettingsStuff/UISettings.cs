using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "GlobalSettings/UISettings")]
    public class UISettings : ScriptableObject
    {
        [SerializeField] private float _openDuration = 1f;
        [SerializeField] private float _closeDuration = 1f;

        public float CloseDuration => _closeDuration;

        public float OpenDuration => _openDuration;
    }
}