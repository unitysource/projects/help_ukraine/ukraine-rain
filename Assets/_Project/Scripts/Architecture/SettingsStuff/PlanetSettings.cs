using System.Collections.Generic;
using _Project.Scripts.Tiles;
using NaughtyAttributes;
using UnityEngine;

namespace _Project.Scripts.SettingsStuff
{
    [CreateAssetMenu(fileName = "PlanetSettings", menuName = "Settings/PlanetSettings")]
    public class PlanetSettings : ScriptableObject
    {
        [SerializeField] private StartTilesConfiguration _startTilesConfiguration;

        [SerializeField, Label("Open Tile IDs"),
         ShowIf("_startTilesConfiguration", StartTilesConfiguration.Customizable)]
        private int[] _openTileIDs;

        [SerializeField] private int[] _canBeOpenedInStartTileIDs;
        
        public int[] CanBeOpenedInStartTileIDs => _canBeOpenedInStartTileIDs;

        public int[] OpenTileIDs => _openTileIDs;

        public StartTilesConfiguration TilesConfiguration => _startTilesConfiguration;
    }

    public enum StartTilesConfiguration
    {
        Customizable,
        All
    }
}