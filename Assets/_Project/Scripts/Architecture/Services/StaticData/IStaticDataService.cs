using UnityEngine;

public interface IStaticDataService
{
    T GetStaticData<T>(string path) where T : ScriptableObject;
}