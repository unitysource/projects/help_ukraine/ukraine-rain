using System.IO;
using UnityEditor;
using UnityEngine;

namespace _Project.Scripts.Architecture.Services.SaveLoadService
{
    public static class CustomEditorMenuItem
    {
        [MenuItem("Data/Open data folder &o")]
        private static void OpenDataFolder()
        {
            Debug.Log($"{Application.persistentDataPath}Open folder: ");
            if (!Directory.Exists(Application.persistentDataPath)) return;

            EditorUtility.RevealInFinder(Application.persistentDataPath);

            // Win
            /*System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo
            {
                Arguments = string.Format("/C start {0}", Application.persistentDataPath),
                FileName = "explorer.exe"
            };*/

            // Mac
            System.Diagnostics.Process.Start(Application.persistentDataPath);
        }

        [MenuItem("Data/Clear all data &;")]
        private static void ClearAllData()
        {
            PlayerPrefs.DeleteAll();

            if (Directory.Exists(Application.persistentDataPath))
            {
                string[] files = Directory.GetFiles(Application.persistentDataPath);

                foreach (var file in files)
                {
                    File.Delete(file);
                    Debug.Log($"File '{file}' has been deleted.");
                }
            }
        }
    }
}