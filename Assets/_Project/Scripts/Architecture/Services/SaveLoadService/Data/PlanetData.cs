using System.Collections.Generic;
using _Project.Scripts.Tiles;

namespace _Project.Scripts.Architecture.Services.SaveLoadService.Data
{
    public class PlanetData
    {
        public List<int> CashedOpenTiles = new List<int>();
        public List<OpenTileProperties> FollowingTiles = new List<OpenTileProperties>();
    }
}