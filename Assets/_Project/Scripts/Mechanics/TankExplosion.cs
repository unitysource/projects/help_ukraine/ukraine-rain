﻿using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class TankExplosion : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        private static readonly int Explode = Animator.StringToHash("Explode");

        public void Animate()
        {
            gameObject.SetActive(true);
            _animator.SetTrigger(Explode);
        }
    }
}