﻿using _Project.Scripts.Mechanics;

public class TileProperties
{
    private TileProperties(TileDirection tileDirection, TileColor tileColor)
    {
        TileDirection = tileDirection;
        TileColor = tileColor;
    }

    private TileProperties()
    {
        TileDirection = default;
        TileColor = default;
    }

    public static TileProperties Create(TileDirection tileDirection, TileColor tileColor) => 
        new TileProperties(tileDirection, tileColor);
    
    public static TileProperties Create() => 
        new TileProperties();

    public TileDirection TileDirection { get; set; }
    public TileColor TileColor { get; set; }
}