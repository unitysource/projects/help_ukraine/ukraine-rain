﻿using System;
using _Project.Scripts.Architecture.Services;
using DG.Tweening;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class CompleteController : MonoBehaviour
    {
        [SerializeField] private Transform _uiCanvas;
        
        [SerializeField] private float _startRainDelay;
        [SerializeField] private float _startExplosionDelay;
        [SerializeField] private float _startAnimateExplosionDelay;
        [SerializeField] private float _finalDelay;

        [SerializeField] private Animator _bgAnimator;
        [SerializeField] private Animator _tankFogAnimator;
        [SerializeField] private GameObject _wholeTank;
        [SerializeField] private GameObject _explosionTank;

        [SerializeField] private HeartController _heartController;
        [SerializeField] private HelicopterController _helicopterController;
        [SerializeField] private TankExplode _tankExplode;
        [SerializeField] private TankExplosion _tankExplosion;

        private (CompleteState, bool) _completeState;
        private float _timer;

        private static readonly int Stop = Animator.StringToHash("Stop");
        private IUIService _uiService;
        private static readonly int Move = Animator.StringToHash("Move");

        [Inject]
        private void GiveMeConstruct(IUIService uiService)
        {
            _uiService = uiService;
        }

        private void Start()
        {
            _heartController.Initialize(this);
            _helicopterController.Initialize(this);
            
            _uiService.Initialize(_uiCanvas);
        }

        private void Update()
        {
            switch (_completeState.Item1)
            {
                case CompleteState.MovementState:
                    EnterAction(() =>
                    {
                        _wholeTank.SetActive(true);
                        _wholeTank.transform.DOMoveX(1f, 2.5f).onComplete += () => 
                            _bgAnimator.SetTrigger(Move);
                        _explosionTank.SetActive(false);
                        _tankFogAnimator.gameObject.SetActive(false);
                    });

                    if (_timer >= _startRainDelay)
                        ChangeState(CompleteState.HeartsRainState);

                    _timer += Time.deltaTime;
                    break;
                case CompleteState.HeartsRainState:
                    EnterAction(() => { _heartController.CanWork = true; });

                    if (_timer >= _startExplosionDelay)
                        ChangeState(CompleteState.ExplodeState);

                    _timer += Time.deltaTime;
                    break;
                case CompleteState.ExplodeState:
                    EnterAction(() =>
                    {
                        _wholeTank.SetActive(false);
                        _explosionTank.SetActive(true);
                        _bgAnimator.SetTrigger(Stop);
                        _heartController.CanWork = false;
                        _heartController.RemoveAll();
                        _tankExplosion.Animate();
                        _tankExplode.Explode();
                    });

                    if (_timer >= _startAnimateExplosionDelay)
                        ChangeState(CompleteState.FinishExplosionState);

                    _timer += Time.deltaTime;
                    break;
                case CompleteState.FinishExplosionState:
                    EnterAction(() =>
                    {
                        _tankExplode.DeSpawn();
                        _tankFogAnimator.gameObject.SetActive(true);
                    });

                    if (_timer >= _finalDelay)
                        ChangeState(CompleteState.EndState);
                    break;
                case CompleteState.EndState:
                    EnterAction(() =>
                    {
                        var donatWindow = _uiService.OpenWindow(WindowType.DonatWindow, true, false);
                        donatWindow.OpenWindow(true);
                    });
                    break;
            }
        }

        public void ChangeState(CompleteState completeState)
        {
            _completeState.Item1 = completeState;
            _completeState.Item2 = false;
        }

        private void EnterAction(Action action = null)
        {
            if (_completeState.Item2) return;

            _completeState.Item2 = true;
            action?.Invoke();
        }
    }

    public enum CompleteState
    {
        MovementState,
        HeartsRainState,
        ExplodeState,
        FinishExplosionState,
        EndState
    }
}