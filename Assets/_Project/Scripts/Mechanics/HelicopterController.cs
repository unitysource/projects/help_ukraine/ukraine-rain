﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class HelicopterController : MonoBehaviour
    {
        [SerializeField] private Transform _helicopter;
        [SerializeField] private MinMaxFloat _minMaxSpawnDuration;
        [SerializeField] private MinMaxFloat _minMaxSpawnYPosition;
        [SerializeField] private MinMaxFloat _minMaxSpeed;
        [SerializeField] private MinMaxFloat _minMaxScale;

        private float _timer;
        private RandomService _randomService;
        private readonly Queue<(Transform, float)> _helicopters = new Queue<(Transform, float)>();
        private CompleteController _completeController;

        [Inject]
        private void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }

        public void Initialize(CompleteController completeController)
        {
            _completeController = completeController;
        }

        private void Start()
        {
            Spawn();
        }

        private void Spawn()
        {
            Transform newHelicopter = Instantiate(_helicopter, new Vector3(4f,
                _randomService.GetValue(_minMaxSpawnYPosition)), Quaternion.identity);
            newHelicopter.localScale = _randomService.GetValue(_minMaxScale) * Vector3.one;

            _helicopters.Enqueue((newHelicopter, _randomService.GetValue(_minMaxSpeed)));
        }

        private void FixedUpdate()
        {
            if (_timer >= _randomService.GetValue(_minMaxSpawnDuration))
            {
                Spawn();
                _timer = 0f;
            }

            foreach ((var trans, float speed) in _helicopters)
            {
                Vector2 position = trans.position;
                trans.Translate(Vector3.left * (speed * Time.deltaTime));
            }

            _timer += Time.fixedDeltaTime;
        }
    }
}