﻿using System;
using _Project.Scripts.Services;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class Explosion : MonoBehaviour
    {
        private RandomService _randomService;

        public void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }
        
        public void Activate()
        {
            transform.DOScale(_randomService.GetValue(0.1f, 0.4f), 0.4f).From(0f);
            DOVirtual.DelayedCall(1f, Deactivate);
        }

        private void Deactivate()
        {
            GetComponent<SpriteRenderer>().DOFade(0f, 0.4f);
        }
    }
}