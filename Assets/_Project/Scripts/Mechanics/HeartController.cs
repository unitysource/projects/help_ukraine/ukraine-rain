﻿using System.Collections.Generic;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using ModestTree;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public class HeartController : MonoBehaviour
    {
        [SerializeField] private Heart _heart;
        [SerializeField] private MinMaxFloat _minMaxSpawnDuration;
        [SerializeField] private MinMaxFloat _minMaxSpawnYPosition;
        [SerializeField] private MinMaxFloat _minMaxRotation;
        [SerializeField] private MinMaxFloat _minMaxForce;
        
        public bool CanWork { get; set; }

        // [SerializeField] private MinMaxFloat _minMaxSpeed;

        private float _timer;
        private RandomService _randomService;
        private CompleteController _completeController;

        private readonly Queue<Heart> _hearts = new Queue<Heart>();

        [Inject]
        private void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }

        public void Initialize(CompleteController completeController)
        {
            _completeController = completeController;
        }

        private void Spawn()
        {
            Heart heart = Instantiate(_heart, new Vector3(4f,
                    _randomService.GetValue(_minMaxSpawnYPosition)),
                Quaternion.Euler(0, 0, _randomService.GetValue(_minMaxRotation)));

            heart.Construct(_randomService);
            heart.Initialize();
            heart.AddForce(_randomService.GetValue(_minMaxForce));
            
            _hearts.Enqueue(heart);
        }

        private void FixedUpdate()
        {
            if (!CanWork) return;

            if (_timer >= _randomService.GetValue(_minMaxSpawnDuration))
            {
                Spawn();
                _timer = 0f;
            }

            _timer += Time.fixedDeltaTime;
        }

        public void RemoveAll()
        {
            if (!_hearts.IsEmpty())
                foreach (Heart heart in _hearts)
                    Destroy(heart.gameObject);
        }
    }
}