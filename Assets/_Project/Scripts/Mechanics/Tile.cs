﻿using System;
using System.Collections.Generic;
using _Project.Scripts.Mechanics;
using _Project.Scripts.SettingsStuff;
using DG.Tweening;
using ModestTree;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[SelectionBase]
public class Tile : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private Image _tearImage;
    [SerializeField] private RectTransform _tearRectTransform;
    [SerializeField] private Image _bgImage;

    private DataService _dataService;
    private Board _board;
    public TileProperties TileProperties { get; private set; }
    public Vector2Int Position { get; set; }
    public int ID { get; set; }

    public bool IsFake => Position.x == -1 || Position.y == -1;
    public RectTransform RectTransform { get; private set; }

    public Vector2 WorldPosition
    {
        get => RectTransform.position;
        private set => RectTransform.position = value;
    }

    public void Construct(DataService dataService, Board board)
    {
        _board = board;
        _dataService = dataService;
    }

    public void Initialize(BoardType boardType)
    {
        RectTransform = GetComponent<RectTransform>();
        IsActive = true;
        TileProperties = TileProperties.Create();

        switch (boardType)
        {
            case BoardType.Board_4x4:
                RectTransform.sizeDelta = Vector2.one * 180;
                break;
            case BoardType.Board_6x4:
                RectTransform.sizeDelta = Vector2.one * 180;
                break;
            case BoardType.Board_6x6:
                RectTransform.sizeDelta = Vector2.one * 144;
                break;
        }
    }

    public void Activate()
    {
        TearRectTransform.gameObject.SetActive(true);
        IsActive = true;
    }

    public void Deactivate(bool isSmooth)
    {
        if (isSmooth)
        {
            const float duration = 0.3f;
            TearRectTransform.DOKill();
            _tearImage.DOKill();

            TearRectTransform.DOScale(TearRectTransform.localScale * 1.1f, duration).onComplete += () =>
                TearRectTransform.DOScale(TearRectTransform.localScale * 1.05f, duration);
            _tearImage.DOFade(0f, duration).onComplete += () =>
                TearRectTransform.gameObject.SetActive(false);
            _bgImage.color = _dataService.WorldSettings.DeactivatedTileColor;
            IsActive = false;
        }
        else
        {
            TearRectTransform.gameObject.SetActive(false);
            _bgImage.color = _dataService.WorldSettings.DeactivatedTileColor;
            IsActive = false;
        }
    }

    public bool IsActive { get; private set; }

    public RectTransform TearRectTransform => _tearRectTransform;

    public bool IsMatchColor(Tile sTile) => TileProperties.TileColor != sTile.TileProperties.TileColor;

    public void SetColor(TileColor color)
    {
        TileProperties.TileColor = color;

        _tearImage.sprite = color switch
        {
            TileColor.Blue => _dataService.WorldSettings.BlueTierSprite,
            TileColor.Yellow => _dataService.WorldSettings.YellowTierSprite,
            _ => _tearImage.sprite
        };
    }

    public void SetDirection(TileDirection direction)
    {
        TileProperties.TileDirection = direction;

        switch (direction)
        {
            case TileDirection.Left:
                TearRectTransform.localEulerAngles = Vector3.zero;
                break;
            case TileDirection.Right:
                RectTransform.localEulerAngles = Vector3.forward * 180;
                TearRectTransform.localScale = new Vector3(1f, -1f, 1f);
                break;
            case TileDirection.Up:
                RectTransform.localEulerAngles = Vector3.forward * 270;
                TearRectTransform.localScale = new Vector3(1f, -1f, 1f);
                break;
            case TileDirection.Down:
                RectTransform.localEulerAngles = Vector3.forward * 90;
                break;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        // _board.AddFake(this);
        _board.Moved = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _board.Move(eventData.delta, this);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (_board.Moved)
            _board.EndDrag();
    }

    public void Connect((Vector2, Vector2Int) point, bool isSmooth)
    {
        var (worldPos, pos) = point;
        Position = pos;
        if (isSmooth)
            RectTransform.DOMove(worldPos, _dataService.WorldSettings.ConnectDuration);
        else
            WorldPosition = worldPos;
    }
}

public enum TileColor
{
    Blue,
    Yellow
}