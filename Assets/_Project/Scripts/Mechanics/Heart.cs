﻿using System;
using _Project.Scripts.Services;
using Assets._Project.Scripts.Utilities.Constants;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Heart : MonoBehaviour
    {
        [SerializeField] private Explosion[] _explosions;

        private Rigidbody2D _rb;
        private RandomService _randomService;

        public void Construct(RandomService randomService)
        {
            _randomService = randomService;
        }

        public void Initialize()
        {
            _rb = GetComponent<Rigidbody2D>();
        }

        public void AddForce(float force)
        {
            _rb.AddForce(Vector2.left * force, ForceMode2D.Impulse);
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag(Tags.Tank))
            {
                // var explosion = Instantiate(_explosions.GetRandomElement(), transform.position, 
                // Quaternion.Euler(0, 0, _randomService.GetValue(-30f, 30f))).GetComponent<Explosion>();
                var explosion = Instantiate(_explosions[0], transform.position, Quaternion.identity);
                // explosion.Construct(_randomService);
                // explosion.Activate();

                gameObject.SetActive(false);
            }
        }
    }
}