using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using _Project.Scripts.Architecture.Services.Pool;
using _Project.Scripts.Architecture.Services.SaveLoadService;
using _Project.Scripts.Services;
using _Project.Scripts.SettingsStuff;
using Assets._Project.Scripts.Utilities.Extensions;
using DG.Tweening;
using ModestTree;
using UnityEngine;
using Zenject;

namespace _Project.Scripts.Mechanics
{
    public enum BoardType
    {
        Board_4x4,
        Board_6x4,
        Board_6x6
    }

    public class Board : MonoBehaviour
    {
        [SerializeField] private RectTransform _fakeParent;
        [SerializeField] private RectTransform[] _borders; // left up right down

        [SerializeField] private Tile _tilePrefab;
        [SerializeField] private RectTransform _bgRectTransform;
        [SerializeField] private Transform _parent;

        [SerializeField] private CanvasGroup _matchBlock;
        [SerializeField] private RectTransform _pointerPrefab;
        private Vector2 _matchBlockPosition;

        private IPoolService _poolService;
        private string _prefabName;
        private Vector2 _startPointPosition;
        private DataService _dataService;
        private const string ContainerName = "TilesContainer";
        private const float MinMoveDelta = 5f;
        private float Del;
        private BoardType _boardType;
        private int _rowsAmount, _colsAmount;

        private readonly List<Tile> _availableTiles = new List<Tile>();


        private List<Tile> _selectedTiles = new List<Tile>();
        private readonly List<(Vector2, Vector2Int)> _points = new List<(Vector2, Vector2Int)>();
        private Borders Borders { get; set; }
        private List<TileProperties> _tileProperties;
        private Core _core;

        private float _maxTilesMoveSpeed;
        private float _offset;
        private RandomService _randomService;
        private ISaveLoadService _saveLoadService;

        public TilesMoveState CurrentMoveState { get; set; }

        public bool Moved { get; set; }

        [Inject]
        private void Construct(IPoolService poolService, DataService dataService,
            RandomService randomService, ISaveLoadService saveLoadService)
        {
            _saveLoadService = saveLoadService;
            _randomService = randomService;
            _dataService = dataService;
            _poolService = poolService;
        }

        public void Initialize(Core core)
        {
            StartCoroutine(CheckWinRoutine());
            _core = core;
            int loopsAmount = 0;
            _matchBlock.alpha = 0f;
            _matchBlockPosition = _matchBlock.transform.position;

            // int levelBoard = (int) (_saveLoadService.PlayerData.Level / 2f);
            // _boardType = levelBoard < Enum.GetValues(typeof(BoardType)).Length
            //     ? (BoardType) levelBoard
            //     : BoardType.Board_6x6;

            _boardType = BoardType.Board_4x4;

            switch (_boardType)
            {
                case BoardType.Board_4x4:
                    _colsAmount = _rowsAmount = 4;
                    _offset = 180;
                    loopsAmount = 2;
                    _startPointPosition = new Vector3(-270, -270);
                    _bgRectTransform.sizeDelta = new Vector2(920, 920);
                    break;
                case BoardType.Board_6x4:
                    _rowsAmount = 6;
                    _colsAmount = 4;
                    _offset = 180;
                    loopsAmount = 3;
                    _startPointPosition = new Vector3(-270, -450);
                    _bgRectTransform.sizeDelta = new Vector2(920, 1270);
                    break;
                case BoardType.Board_6x6:
                    _colsAmount = _rowsAmount = 6;
                    _offset = 143;
                    _startPointPosition = new Vector3(-360, -360);
                    _bgRectTransform.sizeDelta = new Vector2(1050, 1050);
                    loopsAmount = 4;
                    break;
            }

            _maxTilesMoveSpeed = _dataService.WorldSettings.MAXTilesMoveSpeed;
            _prefabName = _tilePrefab.name;

            _tileProperties = new List<TileProperties>(_rowsAmount * _colsAmount);

            for (int i = 0; i < loopsAmount; i++)
            {
                _tileProperties.Add(TileProperties.Create(TileDirection.Left, TileColor.Blue));
                _tileProperties.Add(TileProperties.Create(TileDirection.Right, TileColor.Blue));
                _tileProperties.Add(TileProperties.Create(TileDirection.Up, TileColor.Blue));
                _tileProperties.Add(TileProperties.Create(TileDirection.Down, TileColor.Blue));

                _tileProperties.Add(TileProperties.Create(TileDirection.Left, TileColor.Yellow));
                _tileProperties.Add(TileProperties.Create(TileDirection.Right, TileColor.Yellow));
                _tileProperties.Add(TileProperties.Create(TileDirection.Up, TileColor.Yellow));
                _tileProperties.Add(TileProperties.Create(TileDirection.Down, TileColor.Yellow));
            }

            if (_rowsAmount == 6 && _colsAmount == 6)
            {
                var randDirections = _randomService.GetDifferentValues(2, MinMaxInt.Create(0, 4));

                for (int i = 0; i < 2; i++)
                {
                    _tileProperties.Add(TileProperties.Create((TileDirection) randDirections[i], TileColor.Blue));
                    _tileProperties.Add(TileProperties.Create((TileDirection) randDirections[i], TileColor.Yellow));
                }
            }

            _tileProperties.Shuffle();

            // Borders = new Borders(
            //     new MinMaxFloat(_startPointPosition.x - _offset * 1.5f,
            //         _startPointPosition.x + (_colsAmount + 1) * _offset - _offset / 2),
            //     new MinMaxFloat(_startPointPosition.y - _offset * 1.5f,
            //         _startPointPosition.y + (_rowsAmount + 1) * _offset - _offset / 2));

            Del = _offset / 1.5f;
            float offset = _offset / 220f;
            Borders = new Borders(
                new MinMaxFloat(_borders[0].position.x - offset, _borders[2].position.x + offset),
                new MinMaxFloat(_borders[3].position.y - offset, _borders[1].position.y + offset));

            Debug.Log($"b {Borders.XBorder.MinValue}");

            FillPool();
            PlaceTiles();
        }

        private IEnumerator CheckWinRoutine()
        {
            yield return new WaitForSeconds(5f);

            while (gameObject.activeSelf)
            {
                if (IsAllMatches())
                {
                    DOVirtual.DelayedCall(2.5f, () => _core.Complete());
                    break;
                }

                yield return new WaitForSeconds(0.3f);
            }
        }

        private void FillPool() =>
            _poolService.FillPool(PoolInfo.Create(_prefabName, 100, _tilePrefab.gameObject, ContainerName));

        private void PlaceTiles()
        {
            int counter = 0;
            for (int x = 0; x < _colsAmount; x++)
            {
                for (int y = 0; y < _rowsAmount; y++)
                {
                    Tile tile = GetBaseTile();

                    var position = new Vector2(_startPointPosition.x + x * _offset,
                        _startPointPosition.y + y * _offset);
                    tile.transform.localPosition = position;
                    tile.gameObject.SetActive(true);
                    tile.Activate();

                    tile.SetColor(_tileProperties[counter].TileColor);
                    tile.SetDirection(_tileProperties[counter].TileDirection);

                    tile.ID = counter;
                    tile.Position = new Vector2Int(x, y);
                    tile.name = $"tile[{tile.Position}]";
                    _availableTiles.Add(tile);
                    _points.Add((tile.WorldPosition, tile.Position));

                    var fakeTile = GetBaseTile();

                    if (x == 0)
                    {
                        Vector3 pos = new Vector2(_startPointPosition.x + _offset * 4,
                            _startPointPosition.y + y * _offset);
                        fakeTile.RectTransform.localPosition = pos;
                        // SetFakeTile(ref fakeTile, tile, pos);
                        // fakeTile.Position = new Vector2Int(-1, y);
                        _points.Add((fakeTile.WorldPosition, new Vector2Int(-1, y)));
                    }

                    if (x == _colsAmount - 1)
                    {
                        Vector3 pos = new Vector2(_startPointPosition.x + _offset * -1,
                            _startPointPosition.y + y * _offset);
                        // var fakeTile = GetBaseTile();
                        // SetFakeTile(ref fakeTile, tile, pos);
                        // fakeTile.Position = new Vector2Int(-1, y);
                        fakeTile.RectTransform.localPosition = pos;
                        _points.Add((fakeTile.WorldPosition, new Vector2Int(-1, y)));
                    }

                    if (y == 0)
                    {
                        Vector3 pos = new Vector2(_startPointPosition.x + _offset * x,
                            _startPointPosition.y + _offset * 4);
                        // var fakeTile = GetBaseTile();
                        // SetFakeTile(ref fakeTile, tile, pos);
                        // fakeTile.Position = new Vector2Int(x, -1);
                        fakeTile.RectTransform.localPosition = pos;
                        _points.Add((fakeTile.WorldPosition, new Vector2Int(x, -1)));
                    }

                    if (y == _rowsAmount - 1)
                    {
                        Vector3 pos = new Vector2(_startPointPosition.x + _offset * x,
                            _startPointPosition.y + _offset * -1);
                        // var fakeTile = GetBaseTile();
                        // SetFakeTile(ref fakeTile, tile, pos);
                        // fakeTile.Position = new Vector2Int(x, -1);
                        fakeTile.RectTransform.localPosition = pos;
                        _points.Add((fakeTile.WorldPosition, new Vector2Int(x, -1)));
                    }

                    Destroy(fakeTile.gameObject);

                    counter++;
                }
            }
        }

        private void SetFakeTile(ref Tile fakeTile, Tile tile, Vector3 pos)
        {
            fakeTile.transform.localPosition = pos;
            fakeTile.Activate();
            fakeTile.SetColor(tile.TileProperties.TileColor);
            fakeTile.SetDirection(tile.TileProperties.TileDirection);
            // fakeTile.name = $"tile[{fakeTile.Position}]";
            _availableTiles.Add(fakeTile);
            fakeTile.gameObject.SetActive(true);
            // tile.ConnectedFakeTiles.Add(fakeTile);
            // fakeTile.ConnectedFakeTiles.Add(tile);
        }

        private Tile GetBaseTile()
        {
            Tile tile = _poolService.GetPoolObject(_prefabName).GetComponent<Tile>();
            tile.transform.SetParent(_parent, false);
            tile.Construct(_dataService, this);
            tile.Initialize(_boardType);
            return tile;
        }

        public void Move(Vector2 delta, Tile tile)
        {
            if (!Moved) return;

            if (Mathf.Abs(delta.x) >= MinMoveDelta && CurrentMoveState != TilesMoveState.Col)
            {
                if (_selectedTiles.IsEmpty())
                {
                    SetRow(tile);
                    AddFake(Axis.X);
                    AddToChild(_selectedTiles);
                }
                else
                {
                    var moveSpeed = Mathf.Clamp(delta.x, -_maxTilesMoveSpeed, _maxTilesMoveSpeed);
                    _fakeParent.localPosition += new Vector3(moveSpeed, 0, 0);
                }

                // foreach (var t in _selectedTiles)
                // {
                //     var currentTile = t;
                //     // currentTile.RectTransform.localPosition += new Vector3(moveSpeed, 0, 0);
                //
                //     float worldPositionX = currentTile.WorldPosition.x;
                //     float xBorderMinValue = Borders.XBorder.MinValue;
                //     if (worldPositionX <= xBorderMinValue)
                //     {
                //         currentTile.WorldPosition = new Vector2(Borders.XBorder.MaxValue, currentTile.WorldPosition.y);
                //         _fakeParent.DOLocalMove(new Vector3(-Del, 0, 0), _dataService.WorldSettings.ConnectDuration);
                //         ChangeFakeTile(ref currentTile, Axis.X, false);
                //     }
                //     else if (worldPositionX >= Borders.XBorder.MaxValue)
                //     {
                //         currentTile.WorldPosition = new Vector2(xBorderMinValue, currentTile.WorldPosition.y);
                //         _fakeParent.DOLocalMove(new Vector3(Del, 0, 0), _dataService.WorldSettings.ConnectDuration);
                //         ChangeFakeTile(ref currentTile, Axis.X, true);
                //     }
                // }

                CurrentMoveState = TilesMoveState.Row;
            }
            else if (Mathf.Abs(delta.y) >= MinMoveDelta && CurrentMoveState != TilesMoveState.Row)
            {
                if (_selectedTiles.IsEmpty())
                {
                    SetCol(tile);
                    AddFake(Axis.Y);
                    AddToChild(_selectedTiles);
                }
                else
                {
                    var moveSpeed = Mathf.Clamp(delta.y, -_maxTilesMoveSpeed, _maxTilesMoveSpeed);
                    _fakeParent.localPosition += new Vector3(0, moveSpeed, 0);
                }
                
                // foreach (var t in _selectedTiles)
                // {
                //     var curTile = t;
                //     // curTile.RectTransform.localPosition += new Vector3(0, moveSpeed, 0);
                //
                //     if (curTile.WorldPosition.y <= Borders.YBorder.MinValue)
                //     {
                //         curTile.WorldPosition = new Vector2(curTile.WorldPosition.x, Borders.YBorder.MaxValue);
                //         _fakeParent.DOLocalMove(new Vector3(0, -Del, 0), _dataService.WorldSettings.ConnectDuration);
                //         ChangeFakeTile(ref curTile, Axis.Y, false);
                //     }
                //     else if (curTile.WorldPosition.y >= Borders.YBorder.MaxValue)
                //     {
                //         curTile.WorldPosition = new Vector2(curTile.WorldPosition.x, Borders.YBorder.MinValue);
                //         _fakeParent.DOLocalMove(new Vector3(0, Del, 0), _dataService.WorldSettings.ConnectDuration);
                //         ChangeFakeTile(ref curTile, Axis.Y, true);
                //     }
                // }

                CurrentMoveState = TilesMoveState.Col;
            }
        }

        public void EndDrag()
        {
            CurrentMoveState = TilesMoveState.None;
            Moved = false;

            ClearChildren();

            ConnectTiles();
            CheckMatches();
            RemoveFake();

            _fakeParent.anchoredPosition = Vector2.zero;
            _selectedTiles.Clear();
        }

        private void ClearChildren()
        {
            int childCount = _fakeParent.transform.childCount;
            var children = new List<Transform>(childCount);
            for (int i = 0; i < childCount; i++)
                children.Add(_fakeParent.transform.GetChild(i));
            foreach (var child in children)
                child.SetParent(_parent);
        }

        private void AddFake(Axis axis)
        {
            var tiles = new List<Tile>();
            for (int i = -2; i < 3; i++)
            {
                if (i == 0) continue;

                foreach (var t in _selectedTiles)
                    tiles.Add(PlaceFakeTiles(t, i, axis));
            }

            _selectedTiles.AddRange(tiles);
        }

        private Tile PlaceFakeTiles(Tile t, int i, Axis axis)
        {
            Vector3 pos;
            var fakeTile = GetBaseTile();
            if (axis == Axis.X)
            {
                var localPosition = t.RectTransform.localPosition;
                pos = new Vector2(localPosition.x + i * _offset * _colsAmount, localPosition.y);
                fakeTile.Position = new Vector2Int(-1, t.Position.y);
            }
            else
            {
                var localPosition = t.RectTransform.localPosition;
                pos = new Vector2(localPosition.x, localPosition.y + i * _offset * _rowsAmount);
                fakeTile.Position = new Vector2Int(t.Position.x, -1);
            }

            SetFakeTile(ref fakeTile, t, pos);

            if (!t.IsActive)
                fakeTile.Deactivate(false);

            return fakeTile;
        }

        /* private void ChangeFakeTile(ref Tile t, Axis axis, bool isMax)
        {
            Tile nextTile = isMax ? GetMaxNextTile(axis) : GetMinNextTile(axis);

            if (!nextTile.IsActive)
            {
                t.Deactivate(false);
            }
            else
            {
                t.SetColor(nextTile.TileProperties.TileColor);
                t.SetDirection(nextTile.TileProperties.TileDirection);
            }
        }

        private Tile GetMinNextTile(Axis axis)
        {
            float minValue = 10000f;
            Tile preMinT = null;
            foreach (var tile in _selectedTiles)
            {
                if (axis == Axis.X)
                {
                    float worldPositionX = tile.WorldPosition.x;
                    if (worldPositionX <= minValue)
                    {
                        minValue = worldPositionX;
                        preMinT = tile;
                    }
                }
                else
                {
                    float worldPositionY = tile.WorldPosition.y;
                    if (worldPositionY <= minValue)
                    {
                        minValue = worldPositionY;
                        preMinT = tile;
                    }
                }
            }

            var maxT = preMinT;
            minValue = 10000f;
            preMinT = null;

            foreach (var tile in _selectedTiles)
            {
                if (axis == Axis.X)
                {
                    float worldPositionX = tile.WorldPosition.x;
                    if (worldPositionX <= minValue && tile != maxT)
                    {
                        minValue = worldPositionX;
                        preMinT = tile;
                    }
                }
                else
                {
                    float worldPositionY = tile.WorldPosition.y;
                    if (worldPositionY <= minValue && tile != maxT)
                    {
                        minValue = worldPositionY;
                        preMinT = tile;
                    }
                }
            }

            return preMinT;
        }

        private Tile GetMaxNextTile(Axis axis)
        {
            float maxValue = -10000f;
            Tile preMaxT = null;
            foreach (var tile in _selectedTiles)
            {
                if (axis == Axis.X)
                {
                    float worldPositionX = tile.WorldPosition.x;
                    if (worldPositionX >= maxValue)
                    {
                        maxValue = worldPositionX;
                        preMaxT = tile;
                    }
                }
                else
                {
                    float worldPositionY = tile.WorldPosition.y;
                    if (worldPositionY >= maxValue)
                    {
                        maxValue = worldPositionY;
                        preMaxT = tile;
                    }
                }
            }

            var maxT = preMaxT;
            maxValue = -10000f;
            preMaxT = null;

            foreach (var tile in _selectedTiles)
            {
                if (axis == Axis.X)
                {
                    float worldPositionX = tile.WorldPosition.x;
                    if (worldPositionX >= maxValue && tile != maxT)
                    {
                        maxValue = worldPositionX;
                        preMaxT = tile;
                    }
                }
                else
                {
                    float worldPositionY = tile.WorldPosition.y;
                    if (worldPositionY >= maxValue && tile != maxT)
                    {
                        maxValue = worldPositionY;
                        preMaxT = tile;
                    }
                }
            }

            return preMaxT;
        }
    */

        private void SetRow(Tile tile) =>
            _selectedTiles.AddRange(_availableTiles.Where(t => t.Position.y == tile.Position.y));

        private void AddToChild(List<Tile> tiles)
        {
            if (_fakeParent.transform.childCount != 0) return;

            tiles.ForEach(t => t.transform.SetParent(_fakeParent.transform));
        }

        private void SetCol(Tile tile) => 
            _selectedTiles.AddRange(_availableTiles.Where(t => t.Position.x == tile.Position.x));

        private void ConnectTiles()
        {
            foreach (Tile selectedTile in _selectedTiles)
            {
                if (selectedTile != null)
                {
                    var point = FindNearestPoint(selectedTile);
                    selectedTile.Connect(point, true);
                }
            }
        }

        private (Vector2, Vector2Int) FindNearestPoint(Tile tile)
        {
            float nearestDistance = 10000f;
            (Vector2, Vector2Int) nearestPoint = (default, default);

            foreach (var point in _points)
            {
                float dist = Vector2.Distance(point.Item1, tile.WorldPosition);
                if (dist <= nearestDistance)
                {
                    nearestDistance = dist;
                    nearestPoint = point;
                }
            }

            return nearestPoint;
        }

        public void CheckMatches()
        {
            if (_selectedTiles.IsEmpty()) return;

            foreach (Tile tile in _selectedTiles)
                FindMatches(tile);
        }

        private void FindMatches(Tile tile)
        {
            if (tile.Position.x != 0)
            {
                Tile sTile = _availableTiles.Find(
                    t => t.Position.y == tile.Position.y &&
                         t.Position.x == tile.Position.x - 1);
                if (CheckMatchConditions(tile, sTile, TileDirection.Right, TileDirection.Left))
                    MatchTiles(tile, sTile);
            }

            if (tile.Position.x != _colsAmount - 1)
            {
                Tile sTile = _availableTiles.Find(
                    t => t.Position.y == tile.Position.y &&
                         t.Position.x == tile.Position.x + 1);
                if (CheckMatchConditions(tile, sTile, TileDirection.Left, TileDirection.Right))
                    MatchTiles(tile, sTile);
            }

            if (tile.Position.y != 0)
            {
                Tile sTile = _availableTiles.Find(
                    t => t.Position.x == tile.Position.x &&
                         t.Position.y == tile.Position.y - 1);
                if (CheckMatchConditions(tile, sTile, TileDirection.Up, TileDirection.Down))
                    MatchTiles(tile, sTile);
            }

            if (tile.Position.y != _rowsAmount - 1)
            {
                Tile sTile = _availableTiles.Find(
                    t => t.Position.x == tile.Position.x &&
                         t.Position.y == tile.Position.y + 1);
                if (CheckMatchConditions(tile, sTile, TileDirection.Down, TileDirection.Up))
                    MatchTiles(tile, sTile);
            }
        }

        private static bool CheckMatchConditions(Tile tile, Tile sTile,
            TileDirection sTileDirection, TileDirection tileDirection) =>
            tile != null && sTile != null &&
            !tile.IsFake
            && !sTile.IsFake
            && sTile.TileProperties.TileDirection == sTileDirection &&
            tile.TileProperties.TileDirection == tileDirection
            && tile.IsMatchColor(sTile) && tile.IsActive && sTile.IsActive;

        private void MatchTiles(Tile tile, Tile sTile)
        {
            tile.Deactivate(true);
            sTile.Deactivate(true);

            var _pointer = Instantiate(_pointerPrefab, _core.UICanvas);
            _pointer.transform.position = tile.TearRectTransform.transform.position;
            _pointer.DOMove(_matchBlockPosition, 1f).onComplete += () =>
            {
                const float duration = 0.3f;
                _matchBlock.DOKill();
                _matchBlock.DOFade(1f, duration).From(0f).SetEase(Ease.InOutSine);
                DOVirtual.DelayedCall(2f, () =>
                {
                    _matchBlock.DOFade(0f, duration);
                    _pointer.GetComponent<CanvasGroup>().DOFade(0f, duration);
                });
            };
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                _core.Complete();
            }
        }

        private bool IsAllMatches()
        {
            return _availableTiles.All(t => !t.IsActive);
        }

        public void RemoveFake()
        {
            var fakeTiles = _availableTiles.Where(tile => tile.IsFake);
            foreach (var tile in fakeTiles) Destroy(tile.gameObject);

            _availableTiles.RemoveAll(tile => tile.IsFake);
        }
    }

    public class Borders
    {
        public MinMaxFloat XBorder { get; }

        public MinMaxFloat YBorder { get; }

        public Borders(MinMaxFloat xBorder, MinMaxFloat yBorder)
        {
            XBorder = xBorder;
            YBorder = yBorder;
        }
    }

    public enum TilesMoveState
    {
        None,
        Col,
        Row
    }

    public enum TileDirection
    {
        Left,
        Right,
        Up,
        Down
    }
}