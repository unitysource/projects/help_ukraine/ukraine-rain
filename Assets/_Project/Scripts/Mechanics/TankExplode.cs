﻿using System;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts.Mechanics
{
    public class TankExplode : MonoBehaviour
    {
        [SerializeField] private float _gravityScale;
        [SerializeField] private Rigidbody2D[] _parts;

        public void Explode()
        {
            AddGravity();
        }

        public void AddGravity()
        {
            foreach (var part in _parts) 
                part.gravityScale = _gravityScale;
        }

        public void DeSpawn()
        {
            foreach (var part in _parts) 
                part.GetComponent<SpriteRenderer>().DOFade(0f, 0.4f);
        }
    }
}