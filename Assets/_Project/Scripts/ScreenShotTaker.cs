using System.Reflection;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR || UNITY_STANDALONE
using System;
using System.Collections;
using System.Text.RegularExpressions;
#endif

namespace Capsule
{
    public class ScreenShotTaker : MonoBehaviour
    {
        private void Awake()
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            DontDestroyOnLoad(gameObject);
#else
            Destroy( this );
#endif
        }

#if UNITY_EDITOR || UNITY_STANDALONE
        [Header("Hotkey to save screenshot in Assets folder")]
        [SerializeField]
        private KeyCode defaultCaptureHotkey = KeyCode.F11;
        [SerializeField]
        private KeyCode resizedCaptureHotkey = KeyCode.F12;
        [Space(10.0f)]
        public Resolution[] resolutions =
        {
            new Resolution(750, 1334, 1),
            new Resolution(1242, 2688 , 1),
            new Resolution(1242, 2208, 1),
            new Resolution(2048, 2732, 1)
        };

        [Header("Allow only for similar aspect ratios")]
        public bool lockingAspects = false;

        private bool canGetShot = true;

        private void Update()
        {
            if (Input.GetKeyUp(defaultCaptureHotkey)) TakeDefaultCreenShot();
            if (Input.GetKeyUp(resizedCaptureHotkey)) TakeScreenShot();
        }

        public void TakeScreenShot()
        {
            if (canGetShot)
            {
                canGetShot = false;
                StartCoroutine(ScreenCaptureAction());
            }
        }

        private void TakeDefaultCreenShot()
        {
            string shotName = " Sccreenshot" + DateTime.Now.ToString("HH-mm-ss");
            Debug.Log("Screenshot taken: " + shotName);
            string resolshot = shotName + string.Format("{0}x{1}", Screen.width, Screen.height) + ".png";
            ApplyScreenshot(resolshot, 1);
        }

        private IEnumerator ScreenCaptureAction()
        {
            string shotName = " Sccreenshot" + DateTime.Now.ToString("HH-mm-ss");
            Debug.Log("Screenshot taken: " + shotName);
#if UNITY_EDITOR
            int defaultResolution = Resolution.GetResolutionIndex();
#else
            ResolutionCheckerStandalone.change_size = false;
            Resolution defaultResolution = Resolution.ScreenResolution();
#endif
            float defaultAspect = Resolution.ScreenAscpect();
            bool isHorizontal = defaultAspect > 1.0f;
            yield return new WaitForEndOfFrame();
            for (int i = 0; i < resolutions.Length; i++)
            {
                yield return new WaitForSeconds(0.05f);
                if (resolutions[i].SetScreen(defaultAspect, isHorizontal, lockingAspects))
                {
                    string resolshot = resolutions[i].ToStringMult(isHorizontal) + shotName + ".png";
                    yield return new WaitForSeconds(0.05f);
                    ApplyScreenshot(resolshot, resolutions[i].scale);
                    yield return new WaitForSeconds(0.05f);
                }
            }
#if UNITY_EDITOR
            Resolution.SetScreenResolution(defaultResolution);
#else
            Screen.SetResolution( defaultResolution.width, defaultResolution.height, false );
            ResolutionCheckerStandalone.change_size = true;
#endif
            canGetShot = true;
        }

        private void ApplyScreenshot(string name, int scale)
        {
            ScreenCapture.CaptureScreenshot(name, scale);
        }

        [Serializable]
        public struct Resolution
        {
            public int width;
            public int height;
            public int scale;

            public Resolution(int width, int height, int scale)
            {
                this.width = width;
                this.height = height;
                this.scale = scale;
            }

            public override string ToString()
            {
                return string.Format("{0}x{1}", width, height);
            }

            public string ToString(int multiple)
            {
                return string.Format("{0}x{1}", width * multiple, height * multiple);
            }

            public string ToStringMult(bool isHorizontal)
            {
                if (isHorizontal)
                    return string.Format("{0}x{1}", width * scale, height * scale);
                else
                    return string.Format("{0}x{1}", height * scale, width * scale);
            }

            public static float ScreenAscpect()
            {
                return (float) Screen.width / Screen.height;
            }

            public static Resolution ScreenResolution()
            {
                return new Resolution(Screen.width, Screen.height, 1);
            }

            public bool SetScreen(float screenAspect, bool isHorizontal, bool lock_aspects)
            {
                int orientWidth = isHorizontal ? width : height;
                int orientHeight = isHorizontal ? height : width;

                if (lock_aspects)
                {
                    float requiredAspect = (float) orientWidth / orientHeight;
                    if (Math.Abs(requiredAspect - screenAspect) > 0.1f)
                    {
                        Debug.LogWarning(string.Format(
                            "The current aspect ratio[{0}] is too different from the required ({1}x{2})[{3}]",
                            screenAspect, orientWidth, orientHeight, requiredAspect));
                        return false;
                    }
                }
#if UNITY_EDITOR
                var sizesType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizes");
                var singleType = typeof(ScriptableSingleton<>).MakeGenericType(sizesType);
                var instanceProp = singleType.GetProperty("instance");
                object gameViewSizesInstance = instanceProp.GetValue(null, null);

                MethodInfo getGroup = sizesType.GetMethod("GetGroup");

                GameViewSizeGroupType targetView;
#if UNITY_ANDROID
                targetView = GameViewSizeGroupType.Android;
#elif UNITY_IOS
                targetView = UnityEditor.GameViewSizeGroupType.iOS;
#elif UNITY_STANDALONE
                targetView = UnityEditor.GameViewSizeGroupType.Standalone;
#endif

                var group = getGroup.Invoke(gameViewSizesInstance, new object[] {(int) targetView});
                var getDisplayTexts = group.GetType().GetMethod("GetDisplayTexts");
                var displayTexts = getDisplayTexts.Invoke(group, null) as string[];

                Regex r = new Regex(orientWidth + @"\D" + orientHeight);
                for (int i = 0; i < displayTexts.Length; i++)
                {
                    if (r.Match(displayTexts[i]).Success)
                    {
                        SetScreenResolution(i);
                        return true;
                    }
                }

                int addedSizeId = displayTexts.Length;
                var addCustomSize = getGroup.ReturnType.GetMethod("AddCustomSize"); // or group.GetType().
                var gvsType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSize");
                // var ctor = gvsType.GetConstructor(new[] {typeof(int), typeof(int), typeof(int), typeof(string)});
                var gvstType = typeof(Editor).Assembly.GetType("UnityEditor.GameViewSizeType");
                var ctor = gvsType.GetConstructor(new Type[] { gvstType, typeof(int), typeof(int), typeof(string) });
                var newSize = ctor.Invoke(new object[] { 1, width, height, "1" });
                addCustomSize.Invoke(group, new object[] { newSize });

                SetScreenResolution(addedSizeId);
#else
                Screen.SetResolution( orientWidth, orientHeight, false );
#endif
                return true;
            }
#if UNITY_EDITOR
            public static void SetScreenResolution(int index)
            {
                var gvWndType = typeof(Editor).Assembly.GetType("UnityEditor.GameView");
                var gvWnd = EditorWindow.GetWindow(gvWndType);
                var SizeSelectionCallback = gvWndType.GetMethod("SizeSelectionCallback",
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                SizeSelectionCallback.Invoke(gvWnd, new object[] {index, null});
            }

            public static int GetResolutionIndex()
            {
                var gvWndType = Type.GetType("UnityEditor.GameView,UnityEditor");
                var gvWnd = EditorWindow.GetWindow(gvWndType);
                var get_selectedSizeIndex = gvWndType.GetMethod("get_selectedSizeIndex",
                    BindingFlags.Instance | BindingFlags.NonPublic);
                return (int) get_selectedSizeIndex.Invoke(gvWnd, null);
            }
#endif
        }

#endif
    }
}