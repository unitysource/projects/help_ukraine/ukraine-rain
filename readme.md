# Ukraine Rain

## Table of Contents
- [About the Game](#about-the-game)
- [Gameplay](#gameplay)
- [Features](#features)
- [Getting Started](#getting-started)
- [Installation](#installation)
- [Controls](#controls)
- 
## About the Game

Ukraine Rain is a game created for support Ukrainian army. The game is made using Unity and C#.

![Game Screenshot 1](/images/screenshot1.png)
![Game Screenshot 1](/images/screenshot2.png)
![Game Screenshot 1](/images/screenshot3.png)

## Gameplay

Match harts and destroy vehicles. The game is made using Unity and C#.

## Features

Key features of game:
- Created to support Ukrainian army.

## Getting Started

To download the game, go to the [releases page]

### Installation

1. Clone the repository: `git clone https://gitlab.com/unitysource/projects/help_ukraine/ukraine-rain.git`
2. Navigate to the game directory: `cd ukraine-rain`
3. Install dependencies: `npm install` or `yarn install`

### Controls

Only mobile controls are available at the moment.

**Mobile Controls:**
- Match harts
- Tap to shoot

## Acknowledgments

- [Sound effects by SoundBible](https://www.soundbible.com/)
